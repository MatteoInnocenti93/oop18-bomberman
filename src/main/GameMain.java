package main;

import game.state.GameContext;

/**
 * This class contains the main method. This is the launcher class for Bomberman
 * game. It initializes the {@link MainMenuState}.
 */
public final class GameMain {

    /**
     */
    private GameMain() {
    }

    /**
     * The launcher for the application.
     * 
     * @param args not used
     */
    public static void main(final String... args) {
        new GameContext().requestMainMenuState();
    }

}
