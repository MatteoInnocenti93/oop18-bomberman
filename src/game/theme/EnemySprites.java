package game.theme;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import game.graphics.Sprite;
import game.graphics.SpriteSheet;

/**
 * This class stores any enemy {@link Sprite}.
 */
public class EnemySprites extends AbstractDynamicEntitySprites {

    private static final int SPRITES_TO_MOVE = 4;
    private static final int SPRITES_TO_STAY = 2;
    private static final int Y_LOCATION_MOVE_LEFT = 0;
    private static final int Y_LOCATION_MOVE_RIGHT = 1;
    private static final int Y_LOCATION_MOVE_UP = 2;
    private static final int Y_LOCATION_MOVE_DOWN = 3;
    private static final int Y_LOCATION_STOP = 4;

    /**
     * Creates {@code EnemySprites} container.
     *
     * @param sheet {@link SpriteSheet} where to take any sprite
     * @param coordinateY that indicates the starting Y position in {@link SpriteSheet} to take enemy {@link Sprite}
     */
    public EnemySprites(final SpriteSheet sheet, final int coordinateY) {
        super();
        IntStream.range(0, SPRITES_TO_STAY).mapToObj(a -> new Sprite(sheet, a + 1, Y_LOCATION_STOP + coordinateY))
                                           .collect(Collectors.toList())
                                           .forEach(a -> super.getStopSprites().add(a));
        for (int i = 0; i < SPRITES_TO_MOVE; i++) {
            super.getRightSprites().add(new Sprite(sheet, i + 1, Y_LOCATION_MOVE_RIGHT + coordinateY));
            super.getLeftSprites().add(new Sprite(sheet, i + 1, Y_LOCATION_MOVE_LEFT + coordinateY));
            super.getDownSprites().add(new Sprite(sheet, i + 1, Y_LOCATION_MOVE_DOWN + coordinateY));
            super.getUpSprites().add(new Sprite(sheet, i + 1, Y_LOCATION_MOVE_UP + coordinateY));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSpritesNumberToMove() {
        return SPRITES_TO_MOVE;
    }

}
