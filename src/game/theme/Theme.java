package game.theme;

/**
 * It models a theme.
 */
public interface Theme {

    /**
     * Gets the container of all sounds.
     *
     * @return {@link SoundsManager} with all sounds within
     */
    SoundsManager getSounds();

    /**
     * Gets the container of all sprites.
     *
     * @return {@link SpritesManager} with all sprites within
     */
    SpritesManager getSprites();

}
