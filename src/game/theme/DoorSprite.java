package game.theme;

import game.graphics.Sprite;
import game.graphics.SpriteSheet;

/**
 * This class store any door {@link Sprite}.
 */
public class DoorSprite {

    private static final int Y_LOCATION = 1;
    private static final int OPEN_DOOR_LOCATION = 4;
    private static final int CLOSED_DOOR_LOCATION = 5;

    private final Sprite openDoor;
    private final Sprite closedDoor;

    /**
     * Creates a {@code DoorSprites} container.
     *
     * @param sheet {@link SpriteSheet} where to take the {@link Sprite}
     */
    public DoorSprite(final SpriteSheet sheet) {
        this.openDoor = new Sprite(sheet, OPEN_DOOR_LOCATION, Y_LOCATION);
        this.closedDoor = new Sprite(sheet, CLOSED_DOOR_LOCATION, Y_LOCATION);
    }

    /**
     * Gets the open door sprite.
     *
     * @return open door {@link Sprite}
     */
    public Sprite getOpenDoor() {
        return this.openDoor;
    }

    /**
     * Gets the closed door sprite.
     *
     * @return closed door {@link Sprite}
     */
    public Sprite getClosedDoor() {
        return this.closedDoor;
    }

}
