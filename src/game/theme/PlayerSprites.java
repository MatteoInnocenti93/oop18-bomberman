package game.theme;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import game.graphics.Sprite;
import game.graphics.SpriteSheet;

/**
 * This class stores any player {@link Sprite}.
 */
public class PlayerSprites extends AbstractDynamicEntitySprites {

    private static final int SPRITES_TO_MOVE = 7;
    private static final int SPRITES_TO_STAY = 2;
    private static final int Y_LOCATION_STOP = 2;
    private static final int Y_LOCATION_MOVE_LEFT = 3;
    private static final int Y_LOCATION_MOVE_RIGHT = 4;
    private static final int Y_LOCATION_MOVE_UP = 5;
    private static final int Y_LOCATION_MOVE_DOWN = 6;

    /**
     * Creates {@code PlayerSprites} container.
     *
     * @param sheet {@link SpriteSheet} where to take the {@link Sprite}
     */
    public PlayerSprites(final SpriteSheet sheet) {
        super();
        IntStream.range(0, SPRITES_TO_STAY).mapToObj(a -> new Sprite(sheet, a + 1, Y_LOCATION_STOP))
                                           .collect(Collectors.toList())
                                           .forEach(a -> super.getStopSprites().add(a));
        for (int i = 0; i < SPRITES_TO_MOVE; i++) {
            super.getRightSprites().add(new Sprite(sheet, i + 1, Y_LOCATION_MOVE_RIGHT));
            super.getLeftSprites().add(new Sprite(sheet, i + 1, Y_LOCATION_MOVE_LEFT));
            super.getDownSprites().add(new Sprite(sheet, i + 1, Y_LOCATION_MOVE_DOWN));
            super.getUpSprites().add(new Sprite(sheet, i + 1, Y_LOCATION_MOVE_UP));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSpritesNumberToMove() {
        return SPRITES_TO_MOVE;
    }

}
