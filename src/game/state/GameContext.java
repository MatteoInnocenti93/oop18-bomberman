package game.state;

import java.util.Optional;

/**
 * This is a {@link GameContext}, it returns the states that can be implemented
 * on request. All incoming requests will be handled according to the finite
 * state machine principle. Consequently, this class also takes care of checking
 * that the new state request is not the same as the one in execution.
 */
public class GameContext {

    private Optional<GameState> currentState;
    private final GameState mainMenuState;
    private final GameState chooseThemeState;
    private final GameState gameOverState;
    private final GameState winningState;
    private final GameState infoState;
    private final GameState pauseState;
    private Optional<GameState> runningState;

    /**
     * Creates a {@code GameContext} manager.
     */
    public GameContext() {
        this.currentState = Optional.empty();
        this.mainMenuState = new MainMenuState(this);
        this.chooseThemeState = new ChooseThemeState(this);
        this.gameOverState = new GameOverState(this);
        this.winningState = new WinningState(this);
        this.infoState = new InfoMenuState(this);
        this.pauseState = new PauseState(this);
        this.runningState = Optional.empty();
    }

    /**
     * Request to set the {@link MainMenuState}.
     */
    public void requestMainMenuState() {
        setNewState(this.mainMenuState);
    }

    /**
     * Request to set the {@link ChooseThemeState}.
     */
    public void requestChooseThemeState() {
        setNewState(this.chooseThemeState);
    }

    /**
     * Request to set the {@link GameOverState}.
     */
    public void requestGameOverState() {
        setNewState(this.gameOverState);
    }

    /**
     * Request to set the {@link WinningState}.
     */
    public void requestWinningState() {
        setNewState(this.winningState);
    }

    /**
     * Request to set the {@link InfoMenuState}.
     */
    public void requestInfoState() {
        setNewState(this.infoState);
    }

    /**
     * Request to set the {@link PauseState}.
     */
    public void requestPauseState() {
        setNewState(this.pauseState);
    }

    /**
     * Request to set the {@link RunningState}.
     *
     * @param folder the theme folder to be load.
     */
    public void requestRunningState(final Optional<String> folder) {
        if (folder.isPresent()) {
            this.runningState = Optional.of(new RunningState(this, folder.get()));
            setNewState(this.runningState.get());
        } else {
            setNewState(this.runningState.get());
        }
    }

    private void setNewState(final GameState newState) {
        final String currentStateName = this.currentState.getClass().getSimpleName();
        final String newStateName = newState.getClass().getSimpleName();
        if (!currentStateName.equals(newStateName)) {
            changeState(newState);
        }
    }

    private void changeState(final GameState newState) {
        newState.runState();
        if (this.currentState.isPresent()) {
            this.currentState.get().closeFrameState();
        }
        this.currentState = Optional.of(newState);
        if (newState.equals(this.gameOverState) || newState.equals(this.winningState)) {
            ((RunningState) this.runningState.get()).stopLoop();
        }
    }

}
