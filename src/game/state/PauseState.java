package game.state;

import javax.swing.JFrame;

import menu.frame.GameMenuPause;

/**
 * This class models Game Pause State.
 * It implements {@link GameState}.
 */
public class PauseState implements GameState {

    private final JFrame gameMenuPause;

    /**
     * Creates {@code PauseState}.
     *
     * @param context game-state controller.
     */
    public PauseState(final GameContext context) {
        this.gameMenuPause = new GameMenuPause(context);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void runState() {
        this.gameMenuPause.setVisible(true);
        this.gameMenuPause.requestFocus();
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void closeFrameState() {
        this.gameMenuPause.dispose();
    }

}
