package game.state;

import javax.swing.JFrame;

import menu.frame.ThemeMenu;

/**
 * The {@link GameState} that's run the {@link ThemeMenu}.
 */
public class ChooseThemeState implements GameState {

    private final JFrame themeMenu;

    /**
     * Creates a {@code ChooseThemeState}.
     *
     * @param gameContext the {@link GameContext} handler.
     */
    public ChooseThemeState(final GameContext gameContext) {
        super();
        this.themeMenu = new ThemeMenu(gameContext);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void runState() {
        this.themeMenu.setVisible(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeFrameState() {
        this.themeMenu.dispose();
    }

}
