package game.input;

/**
 * This functional interface models a command.
 */
public interface Command {

    /**
     * Executes the command operation.
     */
    void execute();
}
