package game.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Optional;

import game.objects.entities.Player;
import game.physics.Direction;
import game.state.GameContext;

/**
 * KeyInput that implements {@link KeyListener} to manage the running state input.
 */
public class KeyInput implements KeyListener {

    private final Player player;
    private final GameContext context;

    /**
     * Creates {@code KeyInput}.
     *
     * @param player reference  to move
     * @param context controller used to set state Pause
     */
    public KeyInput(final Player player, final GameContext context) {
        this.player = player;
        this.context = context;
    }

    /**
     * It generates a {@link Direction} when specific key is pressed and
     * call specific method on player to move it. If space is pressed
     * {@link Player} drops bomb.
     */
    @Override
    public void keyPressed(final KeyEvent e) {
        final Optional<Direction> way;
        switch (e.getKeyCode()) {
        case KeyEvent.VK_A:
            way = Optional.ofNullable(Direction.LEFT);
            break;
        case KeyEvent.VK_S:
            way = Optional.ofNullable(Direction.DOWN);
            break;
        case KeyEvent.VK_D:
            way = Optional.ofNullable(Direction.RIGHT);
            break;
        case KeyEvent.VK_W:
            way = Optional.ofNullable(Direction.UP);
            break;
        case KeyEvent.VK_SPACE:
            this.dropBomb();
            way = Optional.empty();
            break;
        case KeyEvent.VK_P:
            this.launchPause();
        default:
            way = Optional.ofNullable(Direction.STOP);
            break;
        }
        this.moveEntity(way);
    }

    /**
     * It stops the player when a key button is released.
     */
    @Override
    public void keyReleased(final KeyEvent e) {
        if (e.getKeyCode() != KeyEvent.VK_SPACE) {
            this.player.getInput().stop();
        }
    }

    @Override
    public void keyTyped(final KeyEvent e) {
    }

    private void dropBomb() {
        this.player.getInput().dropBomb(this.player.getPosition());
    }

    private void launchPause() {
        this.context.requestPauseState();
    }

    private void moveEntity(final Optional<Direction> way) {
        if (way.isPresent()) {
            this.player.getInput().move(way.get());
        }
    }

}
