package game.input.graph;

import java.util.List;

import game.physics.Direction;
import game.utilities.Position;

/**
 * This models an implementation of a Graph.
 */
public interface Graph {

    /**Indicates if a node is present or not in graph.
     *
     * @param position of a node
     * @return true if a node is present in the graph, otherwise false
     */
    boolean containsNode(Position position);

    /**
     * Gets a {@link Direction} path from a source node to a destination node.
     *
     * @param source node position
     * @param destination node position
     * @return list of {@link Direction} that links source to destination
     */
    List<Direction> search(Position source, Position destination);

    /**
     * Adds a node to the graph.
     *
     * @param position of the node that has to be added to the graph
     */
    void addNode(Position position);

    /**
     * Adds an edge from source node to destination node.
     *
     * @param sourcePosition {@link Position} of source node
     * @param destinationPosition {@link Position} of destination node
     * @param direction of edge
     */
    void addEdge(Position sourcePosition, Position destinationPosition, Direction direction);

}
