package game.input.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.stream.Collectors;

import game.physics.Direction;
import game.utilities.Position;

/**
 * This is an implementation of {@link Graph}.
 */
public class GraphImpl implements Graph {

    private final Map<Vertex, HashMap<Vertex, Direction>> graph;

    /**
     * Builds {@code GraphImpl}.
     */
    public GraphImpl() {
        this.graph = new LinkedHashMap<>();
    }

    private Vertex createVertex(final Position position) {
        return new Vertex(position);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public boolean containsNode(final Position position) {
        return this.getVertexes().stream().anyMatch(v -> v.getPosition().equals(position));
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void addNode(final Position pos) {
        final Vertex node = this.createVertex(pos);
        this.graph.put(node, new LinkedHashMap<>());
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void addEdge(final Position source, final Position destination, final Direction dir) {
        final Vertex sNode = this.getVertex(source);
        final Vertex dNode = this.getVertex(destination);
        this.graph.get(sNode).put(dNode, dir);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public List<Direction> search(final Position s, final Position d) {
        final Vertex source = this.getVertex(s);
        final Vertex destination = this.getVertex(d);
        this.getVertexes().forEach(a -> a.setColor(NodeColor.WHITE));
        final Queue<Vertex> queue = new LinkedList<>();
        source.setColor(NodeColor.GREY);
        queue.add(source);
        while (!queue.isEmpty()) {
            final Vertex current = queue.poll();
            if (current.equals(destination)) {
                return current.getPath();
            } else {
                    this.getNeighbours(current.getPosition())
                        .stream()
                        .filter(n -> n.getColor().equals(NodeColor.WHITE))
                        .forEach(v -> {
                            v.setColor(NodeColor.GREY);
                            v.setPrevious(current);
                            queue.add(v);
                        });
                    current.setColor(NodeColor.BLACK);
            }
        }
        return Collections.emptyList();
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public String toString() {
        final StringBuilder string = new StringBuilder();
        this.graph.keySet().forEach(a -> string.append("Vertex " + a.toString() + " -> "
                                                        + this.getNeighbours(a.getPosition()) + "\n"));
        return string.toString();
    }

    private Direction getDirection(final Vertex source, final Vertex destination) {
        return this.graph.get(source).get(destination);
    }

    private List<Vertex> getVertexes() {
        return Collections.unmodifiableList(this.graph.keySet().stream().collect(Collectors.toList()));
    }

    private Vertex getVertex(final Position pos) {
        return this.getVertexes().stream().filter(v -> v.getPosition().equals(pos)).findFirst().get();
    }

    private List<Vertex> getNeighbours(final Position pos) {
        final List<Vertex> neighbours = new ArrayList<>();
        final Vertex vertex = this.getVertex(pos);
        this.graph.get(vertex).forEach((K, V) -> neighbours.add(K));
        return neighbours;
    }

    private class Vertex {

        private final Position position;
        private Vertex previous;
        private NodeColor color;

        Vertex(final Position position) {
            this.position = position;
        }

        private void setColor(final NodeColor color) {
            this.color = color;
        }

        private NodeColor getColor() {
            return this.color;
        }

        private void setPrevious(final Vertex previous) {
            this.previous = previous;
        }

        private Vertex getPrevious() {
            return this.previous;
        }

        private Position getPosition() {
            return this.position;
        }

        /*
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((position == null) ? 0 : position.hashCode());
            return result;
        }

        /*
         * @see java.lang.Object#equals()
         */
        @Override
        public boolean equals(final Object obj) {
            Optional<Vertex> posOpt = Optional.empty();
            if (obj instanceof Vertex) {
                posOpt = Optional.of((Vertex) obj);
            }
            return (posOpt.isPresent()) ? this.getPosition().equals(posOpt.get().getPosition()) : false;
        }

        /*
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return this.position.toString();
        }

        private List<Direction> getPath() {
            final List<Direction> directionPath = new ArrayList<>();
            Vertex current = this;
            while (current.getPrevious() != null) {
                final Direction dir = GraphImpl.this.getDirection(current.getPrevious(), current);
                directionPath.add(0, dir);
                current = current.getPrevious();
            }
            GraphImpl.this.getVertexes().forEach(v -> v.setPrevious(null));
            return directionPath;
        }

    }
}
