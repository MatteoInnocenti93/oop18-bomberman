package game.objects.entities;

import java.awt.Graphics2D;

import game.graphics.DynamicEntityGraphicsComponent;
import game.graphics.GraphicsComponent;
import game.input.EnemyInputComponent;
import game.input.RandomInputComponent;
import game.object.Solidity;
import game.theme.EnemySprites;
import game.utilities.Position;
import game.world.World;

/**
 * This class implements Balloom Enemy.
 * It randomly moves in the space, trying to reach the player.
 */
public class BalloomEnemy extends AbstractEnemy {

    private static final double BALLOOM_TILE_PER_SECOND = 2;
    private static final int BALLOOM_SCORE = 10;
    private final EnemyInputComponent randomInputComponent;
    private final GraphicsComponent gfxBalloomComponent;

    /**
     * Creates {@code BalloomEnemy}.
     *
     * @param position {@link Position} of the Balloom Enemy
     * @param breakable {@link Solidity} of the Balloom Enemy
     * @param world that contains Balloom Enemy.
     * @param sprite to draw Balloom Enemy
     */
    public BalloomEnemy(final Position position, final Solidity breakable, final World world, final EnemySprites sprite) {
        super(position, breakable, world);
        this.randomInputComponent = new RandomInputComponent(this, BALLOOM_TILE_PER_SECOND);
        this.gfxBalloomComponent = new DynamicEntityGraphicsComponent(this, sprite);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processInput() {
        this.generateCommand();
        this.randomInputComponent.processInput();
    }

    private void generateCommand() {
        if (super.clockCommand()) {
            this.randomInputComponent.generateCommand();
        }
    }

    /**
     * {@inheritDoc}
     **/
     @Override
     public void update(final long elapsedTime) {
         super.update(elapsedTime);
         super.checkPhysicalCollision();
     }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void render(final Graphics2D g) {
        this.gfxBalloomComponent.render(g);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public int getEnemyScore() {
        return BALLOOM_SCORE;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public double getVelocity() {
        return BALLOOM_TILE_PER_SECOND;
    }

}
