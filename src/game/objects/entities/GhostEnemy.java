package game.objects.entities;

import java.awt.Graphics2D;

import game.graphics.GhostGraphicComponent;
import game.graphics.GraphicsComponent;
import game.input.EnemyInputComponent;
import game.input.RandomInputComponent;
import game.object.Solidity;
import game.theme.EnemySprites;
import game.utilities.Position;
import game.world.World;

/**
 * This class models a Ghost Enemy.
 * It randomly moves, like Balloom Enemy, but it doesn't collide with walls or bomb.
 */

public class GhostEnemy extends AbstractEnemy {

    private static final double GHOST_TILE_PER_SECOND = 1;
    private static final int GHOST_ENEMY_SCORE = 50;
    private final GraphicsComponent gfxEnemyComponent;
    private final EnemyInputComponent randomInputComponent;

    /**
    * Creates {@code GhostEnemy}.
    *
    * @param position {@link Position} of Ghost Enemy
    * @param breakable {@link Solidity} of Ghost Enemy
    * @param world that contains Ghost Enemy
    * @param sprite to draw Ghost Enemy
    */
    public GhostEnemy(final Position position, final Solidity breakable, final World world, final EnemySprites sprite) {
        super(position, breakable, world);
        this.gfxEnemyComponent = new GhostGraphicComponent(this, sprite);
        this.randomInputComponent = new RandomInputComponent(this, GHOST_TILE_PER_SECOND);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processInput() {
        this.generateCommand();
        this.randomInputComponent.processInput();
    }

    private void generateCommand() {
        if (super.clockCommand()) {
            this.randomInputComponent.generateCommand();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final Graphics2D g) {
        this.gfxEnemyComponent.render(g);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public int getEnemyScore() {
        return GHOST_ENEMY_SCORE;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public double getVelocity() {
        return GHOST_TILE_PER_SECOND;
    }

}
