package game.objects.entities;

/**
 * This is an interface for BomberMan Enemies.
 * It extends {@link DynamicObject}.
 * Enemies are the inhabitants of the labyrinth: they kill player touching him.
 */
public interface Enemy extends DynamicObject {

    /**
     * Gets the score of enemy, after its elimination.
     *
     * @return score of enemy
     */
    int getEnemyScore();

    /**
     * Gets velocity of enemy.
     *
     * @return velocity of enemy
     */
    double getVelocity();

}
