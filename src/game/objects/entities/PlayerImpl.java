package game.objects.entities;

import java.awt.Graphics2D;
import java.util.Optional;

import game.engine.GameEngineImpl;
import game.graphics.DynamicEntityGraphicsComponent;
import game.graphics.GraphicsComponent;
import game.input.PlayerInputComponent;
import game.input.PlayerInputComponentImpl;
import game.object.Solidity;
import game.objects.blocks.Door;
import game.objects.blocks.PickableObject;
import game.physics.DynamicPhysicsComponent;
import game.physics.DynamicPhysicsComponentImpl;
import game.theme.PlayerSprites;
import game.utilities.Position;
import game.world.World;

/**
 * This class represents the player entity, with its components (graphics, input and physics).
 */
public class PlayerImpl extends AbstractEntity implements Player {

    private static final int CHECK_WIN_DELAY = GameEngineImpl.FPS / 3;

    private final PlayerInputComponent input;
    private final GraphicsComponent gfx;
    private final DynamicPhysicsComponent physics;
    private Optional<PickableObject> key;
    private final Door door;
    private int doorDelay;

    /**
     * Creates {@code Player}.
     *
     * @param position to set player position
     * @param breakable indicates if the entity is breakable or not
     * @param sprites object that contains every player's sprite
     * @param world reference
     */
    public PlayerImpl(final Position position, final Solidity breakable, final PlayerSprites sprites, final World world) {
        super(position, breakable, world);
        this.input = new PlayerInputComponentImpl(this);
        this.gfx = new DynamicEntityGraphicsComponent(this, sprites);
        this.physics = new DynamicPhysicsComponentImpl(this);
        this.key = world.getKey();
        this.door = world.getDoor();
        this.doorDelay = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(final long elapsedTime) {
        this.physics.move(elapsedTime);
        this.physics.checksCollisions(super.getWorld().getBlocks());
        this.checkEveryCollision();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final Graphics2D g) {
        this.gfx.render(g);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PlayerInputComponent getInput() {
        return this.input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processInput() {
        this.input.processInput();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DynamicPhysicsComponent getPhysics() {
        return this.physics;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void takeKey() {
        if (this.key.isPresent() && this.physics.checkGenericCollision(this.key.get())) {
            this.door.open();
            this.getWorld().removeObject(this.key.get());
            this.key = Optional.empty();
        }
    }

    private void checkEveryCollision() {
        this.checkKey();
        this.checkWinGame();
        this.physics.checkBombCollisions();
    }

    private void checkKey() {
        if (this.key.isPresent() && this.physics.checkGenericCollision(this.key.get())) {
            this.key.get().getAction().execute(this);
        }
    }

    private void checkWinGame() {
        if (this.door.isOpen() && this.physics.checkGenericCollision(door)) {
            this.doorDelay++;
        } else if (this.doorDelay > 0) {
            this.doorDelay = 0;
        }
        if (this.doorDelay >= CHECK_WIN_DELAY) {
            this.getWorld().getGameContext().requestWinningState();
        }
    }

}
