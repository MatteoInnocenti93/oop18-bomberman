package game.objects.entities;

import java.util.stream.Collectors;

import game.engine.GameEngineImpl;
import game.object.Solidity;
import game.physics.DynamicPhysicsComponent;
import game.physics.DynamicPhysicsComponentImpl;
import game.utilities.Position;
import game.world.World;

/**
 * This class provides a skeletal implementation of enemy.
 */
public abstract class AbstractEnemy extends AbstractEntity implements Enemy {

    private static final int MILLISECONDS_BETWEEN_COMMAND = 500;
    private final DynamicPhysicsComponent phyEnemyComponent;
    private static final int ENEMY_DELAY = GameEngineImpl.FPS / 5 + 1;
    private int enemyCollisionDelay;
    private long timer;

    /**
     * Creates {@code AbstractEnemy}.
     *
     * @param position of the enemy.
     * @param breakable {@link Solidity} of the enemy.
     * @param world where enemy is placed on.
     */
    public AbstractEnemy(final Position position, final Solidity breakable, final World world) {
        super(position, breakable, world);
        this.phyEnemyComponent = new DynamicPhysicsComponentImpl(this);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void update(final long elapsedTime) {
        this.timer += elapsedTime;
        this.phyEnemyComponent.move(elapsedTime);
        this.checkPlayerCollision();
    }

    private void checkPlayerCollision() {
        if (this.phyEnemyComponent.checkGenericCollision(super.getWorld().getPlayer())) {
            this.enemyCollisionDelay++;
        } else if (this.enemyCollisionDelay > 0) {
            this.enemyCollisionDelay = 0;
        }
        if (this.enemyCollisionDelay >= ENEMY_DELAY) {
            super.getWorld().getGameContext().requestGameOverState();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DynamicPhysicsComponent getPhysics() {
        return this.phyEnemyComponent;
    }

    /**
     * Checks if it's time for a new Command.
     *
     * @return true if timer is clocked and enemy is in the center of a tile, otherwise false
     */
    protected final boolean clockCommand() {
        return (this.enemyTimerClock(this.timer) && this.isInTileCenter());
    }

    private boolean enemyTimerClock(final long time) {
        final boolean condition = time >= MILLISECONDS_BETWEEN_COMMAND;
        if (condition) {
            this.timer = 0;
        }
        return condition;
    }

    /**
     * Checks if enemy is placed in the center of a tile.
     *
     * @return true if enemy is in the center of a tile, otherwise false.
     */
    protected final boolean isInTileCenter() {
        return this.getPosition().isCentered();
    }

    /**
     * Checks physical collision of the enemy.
     */
    protected final void checkPhysicalCollision() {
        this.phyEnemyComponent.checksCollisions(super.getWorld()
                                                          .getAllGameObjects()
                                                          .stream()
                                                          .filter(o -> this.getWorld().getBlocks().contains(o))
                                                          .collect(Collectors.toSet()));
        this.phyEnemyComponent.checkBombCollisions();
    }

}
