package game.objects.entities;

import game.input.PlayerInputComponent;

/**
 * This is an interface that models a Player in game.
 * It extends {@link DynamicObject}.
 */
public interface Player extends DynamicObject {

    /**
     * Gets the player's input component.
     *
     * @return {@link PlayerInputComponent}
     */
    PlayerInputComponent getInput();

    /**
     * Takes the key and opens the door.
     */
    void takeKey();

}
