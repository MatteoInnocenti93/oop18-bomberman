package game.objects.blocks;

import game.object.AbstractGameObject;
import game.object.Solidity;
import game.utilities.Position;

/**
 *  This class models a still {@link AbstractGameObject}.
 */
public abstract class AbstractStillObject extends AbstractGameObject {

    /**
     * Creates an {@code AbstractStillObject}.
     * 
     * @param breakable the possibility to break the object
     * @param position object's position
     */
    public AbstractStillObject(final Solidity breakable, final Position position) {
        super(breakable, position);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Position getPosition() {
        return new Position(super.getPosition());
    }
}
