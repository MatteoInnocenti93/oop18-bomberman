package game.objects.blocks;

import game.objects.entities.PlayerImpl;

/**
 * This functional interface is created for the propriety of any pickable object.
 */
public interface PowerUpAction {

    /**
     * Performs the object's propriety.
     * 
     * @param p player that takes the power up
     */
    void execute(PlayerImpl p);
}
