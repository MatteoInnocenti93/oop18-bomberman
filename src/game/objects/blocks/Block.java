package game.objects.blocks;

import java.awt.Graphics2D;

import game.graphics.GraphicsComponent;
import game.graphics.Sprite;
import game.graphics.StillObjectGraphicComponent;
import game.object.Solidity;
import game.utilities.Position;

/**
 * This class manages a block.
 */
public class Block extends AbstractStillObject {

    private final GraphicsComponent graphicComponent;
    /**
     * Creates a generic {@code Block}.
     * 
     * @param breakable the possibility to break the block
     * @param position block's position
     * @param sprite block's sprite
     */
    public Block(final Solidity breakable, final Position position, final Sprite sprite) {
        super(breakable, position);
        this.graphicComponent = new StillObjectGraphicComponent(this, sprite);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final Graphics2D g) {
        this.graphicComponent.render(g);
    }

}
