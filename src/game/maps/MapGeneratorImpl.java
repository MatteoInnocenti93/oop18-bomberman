package game.maps;

import java.util.Random;

import game.world.GameObjectFactory;

/**
 * Class that builds a many {@link GameMap} layout and it returns one of them casually.
 */
public class MapGeneratorImpl implements MapGenerator {

    private static final int VERTICAL_MAP = 0;
    private static final int HORIZONTAL_MAP = 1;
    private final GameObjectFactory factory;
    private GameMap map;

    /**
     * Creates a {@code MapGeneratorImpl}.
     *
     * @param factory {@link GameObjectFactory} to build the unbreakable blocks
     */
    public MapGeneratorImpl(final GameObjectFactory factory) {
        this.factory = factory;
        this.initializeRandomMap();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GameMap getMapType() {
        return this.map;
    }

    private void initializeRandomMap() {
        final int numMap = new Random().nextInt(3);
        switch (numMap) {
            case VERTICAL_MAP:
                this.map = new VerticalMap(this.factory);
                break;
            case HORIZONTAL_MAP:
                this.map = new HorizontalMap(this.factory);
                break;
            default:
                this.map = new NormalMap(this.factory);
                break;
        }
    }

}
