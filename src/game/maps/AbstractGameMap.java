package game.maps;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import game.graphics.SpriteSheet;
import game.objects.blocks.Block;
import game.utilities.FrameSizeUtils;
import game.utilities.Position;
import game.world.GameObjectFactory;

/**
 * Class to group the mutual elements of any {@link GameMap}.
 */
public abstract class AbstractGameMap implements GameMap {

    private static final int LOWER_BOUND = 1;
    private static final int NUM_TILES = FrameSizeUtils.NUM_TILES;
    private static final int SPRITE_SIZE = SpriteSheet.SPRITE_SIZE_IN_GAME;

    private final Set<Position> freePositions;
    private final Set<Block> layout;

    /**
     * Creates an {@code AbstractGameMap}.
     *
     * @param factory to create the {@link Block} layout
     */
    public AbstractGameMap(final GameObjectFactory factory) {
        this.layout = new HashSet<>();
        this.freePositions = new HashSet<>();
        this.createMap(factory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Block> getMapLayout() {
        return this.layout;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Position> getFreePositions() {
        return this.freePositions;
    }

    /**
     * Initializes {@link GameMap} layout with blocks.
     *
     * @param factory to create the {@link Block} in the map
     */
    protected abstract void initializeMapLayout(GameObjectFactory factory);

    private void removeBlocksPosition() {
        this.freePositions.removeAll(this.layout.stream().map(b -> b.getPosition()).collect(Collectors.toSet()));
    }

    private void initializeFreePositions() {
        IntStream.range(LOWER_BOUND, NUM_TILES - 1)
                 .mapToObj(x -> IntStream.range(LOWER_BOUND, NUM_TILES - 1)
                 .mapToObj(y -> new Position(y * SPRITE_SIZE, x * SPRITE_SIZE))
                 .collect(Collectors.toSet()))
                 .forEach(s -> this.freePositions.addAll(s));
    }

    private void createMap(final GameObjectFactory factory) {
        this.initializeFreePositions();
        this.initializeMapLayout(factory);
        this.removeBlocksPosition();
    }

}
