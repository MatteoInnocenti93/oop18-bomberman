package game.maps;

import java.util.Set;

import game.objects.blocks.Block;
import game.utilities.Position;

/**
 * Interface to models any still blocks layout in the game map.
 */
public interface GameMap {

    /**
     * Gets a set of blocks that represents the game map.
     *
     * @return map still {@link block} layout generated casually
     */
    Set<Block> getMapLayout();

    /**
     * Gets the free positions according to the type of map.
     * @return a set of free {@link Position}
     */
    Set<Position> getFreePositions();

}
