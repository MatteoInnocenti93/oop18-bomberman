package game.physics;

import java.util.Set;

import game.object.GameObject;

/**
 * Interface that models the methods to check any collision.
 */
public interface CollisionPhysics {

    /**
     * Checks a generic collision with this entity.
     *
     * @param tile object which to check the collision
     * @return true/false if an entity collide with the {@link GameObject} passed
     */
    boolean checkGenericCollision(GameObject tile);

    /**
     * Checks the collisions with a set of game objects.
     *
     * @param objects to check a collision on
     */
    void checksCollisions(Set<? extends GameObject> objects);

}
