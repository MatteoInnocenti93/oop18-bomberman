package game.physics;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

import game.object.GameObject;
import game.object.Solidity;
import game.objects.blocks.Bomb;
import game.objects.blocks.Flame;
import game.world.World;

/**
 * Class to manage {@link FlamePhysicsComponent}.
 */
public class FlamePhysicsComponent extends AbstractPhysicsComponent {

    private final Flame flame;

    /**
     * Creates a {@code FlamePhysicsComponent}.
     *
     * @param flame the {@link Flame} where adding a physic
     */
    public FlamePhysicsComponent(final Flame flame) {
        super(flame);
        this.flame = flame;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void checksCollisions(final Set<? extends GameObject> objects) {
        final World world = this.flame.getBomb().getWorld();
        detonateBomb(world.getBombs(), world);
        killPlayer(world);
        removeBurntObjects(getBurntObjectWithoutBombs(objects, world).iterator(), world);
    }

    private Set<? extends GameObject> getBurntObjectWithoutBombs(final Set<? extends GameObject> objects,
            final World world) {
        return objects.stream().filter(object -> object.isBreakable().equals(Solidity.BREAKABLE))
                               .filter(obj -> !world.getBombs().contains(obj))
                               .filter(a -> a.getBounds().intersects(this.flame.getBounds()))
                               .collect(Collectors.toSet());
    }

    private void removeBurntObjects(final Iterator<? extends GameObject> burntObjects, final World world) {
        while (burntObjects.hasNext()) {
            world.removeObject(burntObjects.next());
        }
    }

    private void detonateBomb(final Collection<Bomb> allBombs, final World world) {
        for (final Bomb bomb : allBombs) {
            if (this.flame.getBounds().intersects(bomb.getBounds())) {
                bomb.getPhysicsComponent().explosion(world);
            }
        }
    }

    private void killPlayer(final World world) {
        if (this.flame.getBounds().intersects(world.getPlayer().getBounds())) {
            world.getGameContext().requestGameOverState();
        }
    }

}
