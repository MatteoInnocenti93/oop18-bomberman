package game.physics;

import java.awt.geom.Rectangle2D;
import java.util.Set;
import java.util.stream.Collectors;

import game.engine.GameEngineImpl;
import game.graphics.SpriteSheet;
import game.object.GameObject;
import game.objects.entities.DynamicObject;
import game.utilities.Pair;
import game.utilities.Position;
import game.utilities.VelocityImpl;
import game.world.WorldImpl;

/**
 * Class to manage any {@link DynamicObject} physics.
 */
public class DynamicPhysicsComponentImpl extends AbstractPhysicsComponent implements DynamicPhysicsComponent {

    private static final double MILLISECONDS_TO_SECONDS = 0.001;
    private static final int POSITION_ADJUSTMENT = 10;
    private static final int HEIGHT_ADJUSTMENT = 5;
    private static final int WIDTH_ADJUSTMENT = 20;

    private final DynamicObject entity;
    private final VelocityImpl vector;
    private boolean isOnBomb;

    /**
     * Creates {@code DynamicPhysicsComponentImpl}.
     *
     * @param entity to manage its physics
     */
    public DynamicPhysicsComponentImpl(final DynamicObject entity) {
        super(entity);
        this.entity = entity;
        this.vector = new VelocityImpl();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checksCollisions(final Set<? extends GameObject> set) {
        set.forEach(wall -> {
            if (wall.getBounds().intersects(this.getTopBound())) {
                this.entity.getPosition().setY(wall.getPosition().getY() + SpriteSheet.SPRITE_SIZE_IN_GAME);
            } else if (wall.getBounds().intersects(this.getLowerBound())) {
                this.entity.getPosition().setY(wall.getPosition().getY() - SpriteSheet.SPRITE_SIZE_IN_GAME);
            } else if (wall.getBounds().intersects(this.getRightBound())) {
                this.entity.getPosition().setX(wall.getPosition().getX() - SpriteSheet.SPRITE_SIZE_IN_GAME);
            } else if (wall.getBounds().intersects(this.getLeftBound())) {
                this.entity.getPosition().setX(wall.getPosition().getX() + SpriteSheet.SPRITE_SIZE_IN_GAME);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Rectangle2D getTopBound() {
        return new Rectangle2D.Double(this.entity.getPosition().getX() + POSITION_ADJUSTMENT,
                                      this.entity.getPosition().getY(),
                                      SpriteSheet.SPRITE_SIZE_IN_GAME - WIDTH_ADJUSTMENT,
                                      HEIGHT_ADJUSTMENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Rectangle2D getLowerBound() {
        return new Rectangle2D.Double(this.entity.getPosition().getX() + POSITION_ADJUSTMENT,
                                      this.entity.getPosition().getY() + SpriteSheet.SPRITE_SIZE_IN_GAME
                                                                             - HEIGHT_ADJUSTMENT,
                                      SpriteSheet.SPRITE_SIZE_IN_GAME - WIDTH_ADJUSTMENT,
                                      HEIGHT_ADJUSTMENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Rectangle2D getLeftBound() {
        return new Rectangle2D.Double(this.entity.getPosition().getX(),
                                      this.entity.getPosition().getY() + POSITION_ADJUSTMENT,
                                      HEIGHT_ADJUSTMENT,
                                      SpriteSheet.SPRITE_SIZE_IN_GAME - WIDTH_ADJUSTMENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Rectangle2D getRightBound() {
        return new Rectangle2D.Double(this.entity.getPosition().getX() + SpriteSheet.SPRITE_SIZE_IN_GAME
                                                                             - HEIGHT_ADJUSTMENT,
                                      this.entity.getPosition().getY() + POSITION_ADJUSTMENT,
                                      HEIGHT_ADJUSTMENT,
                                      SpriteSheet.SPRITE_SIZE_IN_GAME - WIDTH_ADJUSTMENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VelocityImpl getVelocity() {
        return this.vector;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void checkBombCollisions() {
        if (this.entity.getWorld().getBombs().stream()
                                             .map(m -> m.getPosition())
                                             .anyMatch(p -> p.equals(this.entity.getPosition()))) {
            this.isOnBomb = true;
        }
        if (!this.isOnBomb) {
            this.checksCollisions(this.entity.getWorld().getBombs().stream().collect(Collectors.toSet()));
        }
        this.checkReleaseBomb();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void move(final long elapsedTime) {
        final Position previousPos = this.entity.getPosition();
        final double time = elapsedTime * MILLISECONDS_TO_SECONDS;
        double velX = 0;
        double velY = 0;
        if (time > 0) {
            velX = this.vector.getSpaceX() / time;
            velY = this.vector.getSpaceY() / time;
        }
        final Pair<Double, Double> deltas = this.getDeltas(new Pair<>(velX, velY), time, previousPos);
        this.entity.getPosition().setX(this.entity.getPosition().getX() + deltas.getFirst());
        this.entity.getPosition().setY(this.entity.getPosition().getY() + deltas.getSecond());
    }

    private Pair<Double, Double> getDeltas(final Pair<Double, Double> velocity, final double time, final Position previousPos) {
        double deltaX = (velocity.getFirst() / GameEngineImpl.FPS) * time;
        double deltaY = (velocity.getSecond() / GameEngineImpl.FPS) * time;
        if (previousPos.getX() + deltaX >= WorldImpl.UPPER_BOUND_IN_PIXEL) {
            deltaX = WorldImpl.UPPER_BOUND_IN_PIXEL - previousPos.getX();
        } else if (previousPos.getX() + deltaX <= WorldImpl.LOWER_BOUND_IN_PIXEL) {
            deltaX = WorldImpl.LOWER_BOUND_IN_PIXEL - previousPos.getX();
        }
        if (previousPos.getY() + deltaY >= WorldImpl.UPPER_BOUND_IN_PIXEL) {
            deltaY = WorldImpl.UPPER_BOUND_IN_PIXEL - previousPos.getY();
        } else if (previousPos.getY() + deltaY <= WorldImpl.LOWER_BOUND_IN_PIXEL) {
            deltaY = WorldImpl.LOWER_BOUND_IN_PIXEL - previousPos.getY();
        }
        return new Pair<>(deltaX, deltaY);
    }

    private void checkReleaseBomb() {
        if (this.entity.getWorld().getBombs().stream().filter(a -> this.checkGenericCollision(a)).count() == 0) {
            this.isOnBomb = false;
        }
    }

}
