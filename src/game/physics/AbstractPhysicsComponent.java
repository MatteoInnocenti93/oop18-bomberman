package game.physics;

import java.util.Set;

import game.object.GameObject;

/**
 * Class to manage any alive object physics.
 */
public abstract class AbstractPhysicsComponent implements CollisionPhysics {

    private final GameObject entity;

    /**
     * Creates {@code AbstractPhysicsComponent}.
     *
     * @param entity to manage its physics
     */
    public AbstractPhysicsComponent(final GameObject entity) {
        super();
        this.entity = entity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean checkGenericCollision(final GameObject tile) {
        return tile.getBounds().intersects(this.entity.getBounds());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract void checksCollisions(Set<? extends GameObject> objects);

}
