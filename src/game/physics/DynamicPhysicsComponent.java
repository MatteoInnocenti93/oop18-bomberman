package game.physics;

import java.awt.geom.Rectangle2D;

import game.utilities.VelocityImpl;

/**
 * Interface to models the physics for entities in movement.
 */
public interface DynamicPhysicsComponent extends CollisionPhysics {

    /**
     * Moves the DynamicObject.
     *
     * @param elapsedTime : time between two frames
     */
    void move(long elapsedTime);

    /**
     * Gets an entity upper shape.
     *
     * @return a rectangle to check a upper bound collision
     */
    Rectangle2D getTopBound();

    /**
     * Gets an entity lower shape.
     *
     * @return a rectangle to check a lower bound collision
     */
    Rectangle2D getLowerBound();

    /**
     * Gets an entity left shape.
     *
     * @return a rectangle to check a left bound collision
     */
    Rectangle2D getLeftBound();

    /**
     * Gets an entity right shape.
     *
     * @return a rectangle to check a right bound collision
     */
    Rectangle2D getRightBound();

    /**
     * Gets the dynamic object's velocity.
     *
     * @return the dynamic object {@link VelocityImpl}.
     */
    VelocityImpl getVelocity();

    /**
     * Checks any collisions with bombs.
     */
    void checkBombCollisions();

}
