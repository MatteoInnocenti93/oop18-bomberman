package game.engine;

import javax.swing.SwingUtilities;

import game.state.GameContext;
import game.theme.Theme;
import game.world.World;
import game.world.WorldImpl;

/**
 * This realizes the Game Loop, implementing {@link GameEngine}.
 *
 */
public class GameEngineImpl extends Thread implements GameEngine {

    /**
     * Current game frames per second.
     */
    public static final int FPS = 60;
    private static final int SECOND = 1000;
    private static final int PERIOD = SECOND / FPS;
    private boolean running;
    private boolean updatable;
    private final GameContext context;
    private final Theme theme;
    private World world;
    private Thread thread;

    /**
     * Builds game engine with reference to {@link World}.
     * @param theme world's theme
     * @param context the game context handler {@link GameContext}
     */
    public GameEngineImpl(final Theme theme, final GameContext context) {
        super();
        this.context = context;
        this.theme = theme;
        this.running = false;
        this.updatable = true;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void startLoop() {
        if (!this.running) {
            this.running = true;
            this.world = new WorldImpl(this.theme, this.context);
            this.world.getGameTimer().start();
            this.thread = new Thread(this, "loop");
            this.thread.start();
        }
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void stopLoop() {
        if (this.running) {
            this.running = false;
            this.world.getGraphicComponent().dispose();
            try {
                this.thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void run() {
        long elapsed;
        long currentFrameTime;
        long lastFrameTime = System.currentTimeMillis();
        while (running) {
            currentFrameTime = System.currentTimeMillis();
            elapsed = currentFrameTime - lastFrameTime;
            lastFrameTime = currentFrameTime;
            if (this.updatable) {
                this.processInput();
                this.update(elapsed);
                this.render();
            }
            final long dTime = System.currentTimeMillis() - currentFrameTime;
            this.waitNextFrame(dTime);
        }
        this.stopLoop();
    }

    private void processInput() {
        this.world.processInput();
    }

    private void update(final long elapsed) {
        this.world.update(elapsed);
    }

    private void render() {
        SwingUtilities.invokeLater(() -> {
            this.world.render();
        });
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void setUpdatable() {
        this.updatable = true;
        this.world.getGraphicComponent().setVisible(true);
        this.world.getGraphicComponent().setEnabled(true);
        this.world.getGameTimer().start();
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void unsetUpdatable() {
        this.updatable = false;
        this.world.getGraphicComponent().setEnabled(false);
        this.world.getGameTimer().stop();
    }

    private void waitNextFrame(final long deltaTime) {
        long sleepTime;
        long remainingTime;
        remainingTime = PERIOD - deltaTime;
        if (remainingTime < 0) {
            sleepTime = PERIOD;
        } else {
            sleepTime = remainingTime;
        }
        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
