package game.engine;

/**
 * This interface models the Game Loop, the game engine.
 *
 */
public interface GameEngine {

    /**
     * Starts Game Engine.
     */
    void startLoop();

    /**
     * Stops the Game Engine.
     */
    void stopLoop();

    /**
     * Allows the engine to update the game.
     */
    void setUpdatable();

    /**
     * Forbids the engine to update the game.
     */
    void unsetUpdatable();

}
