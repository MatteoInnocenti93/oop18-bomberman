package game.object;

/**
 * It defines if a game object is breakable or not.
 */
public enum Solidity {

    /**
     * A game object that's breakable, it means that it can
     * be destroyed.
     */
    BREAKABLE,

    /**
     * A game object that's unbreakable, it can be destroyed
     * by nothing.
     */
    UNBREAKABLE;

}
