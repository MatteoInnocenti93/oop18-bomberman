package game.object;


import java.awt.geom.Rectangle2D;

import game.graphics.SpriteSheet;
import game.utilities.Position;

/**
 * This class models a simple {@link GameObject}.
 */
public abstract class AbstractGameObject implements GameObject {

    private final Solidity breakable;
    private final Position position;

    /**
     * Creates a {@code AbstractGameObject}.
     * 
     * @param breakable the possibility to break the object
     * @param position object's position
     */
    public AbstractGameObject(final Solidity breakable, final Position position) {
        this.breakable = breakable;
        this.position = position;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Position getPosition() {
        return this.position;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Solidity isBreakable() {
        return this.breakable;
    }

    /**
     * {@inheritDoc}
     */
    public Rectangle2D getBounds() {
        return new Rectangle2D.Double(this.getPosition().getX(), this.getPosition().getY(),
                   SpriteSheet.SPRITE_SIZE_IN_GAME, SpriteSheet.SPRITE_SIZE_IN_GAME);
    }

}
