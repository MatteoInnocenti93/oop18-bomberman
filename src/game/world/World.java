package game.world;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import game.object.GameObject;
import game.objects.blocks.Block;
import game.objects.blocks.Bomb;
import game.objects.blocks.Door;
import game.objects.blocks.Flame;
import game.objects.blocks.PickableObject;
import game.objects.entities.PlayerImpl;
import game.state.GameContext;
import game.theme.Theme;
import game.utilities.GameTimer;
import game.utilities.PlayerScore;
import game.utilities.Position;
import menu.frame.WorldGraphicComponentImpl;

/**
 * This interface declares the methods of a world.
 */
public interface World {

    /**
     * Updates world's state.
     * 
     * @param elapseTime passed time between two frames
     */
    void update(long elapseTime);

    /**
     * Renders all objects' sprite.
     */
    void render();

    /**
     * Does the process input of any alive object.
     */
    void processInput();

    /**
     * Gets the current theme of the world.
     * 
     * @return the world's theme
     */
    Theme getTheme();

    /**
     * Gets the positions of free tiles.
     * 
     * @return free tiles' positions
     */
    Set<Position> getFreePosition();

    /**
     * Gets all objects that are in game.
     * 
     * @return all game object.
     */
    List<? extends GameObject> getAllGameObjects();

    /**
     * Gets all blocks.
     * 
     * @return a set of all blocks
     */
    Set<Block> getBlocks();

    /**
     * Gets the player.
     * 
     * @return the player
     */
    PlayerImpl getPlayer();

    /**
     * Gets all bombs in game.
     * 
     * @return a queue of all bombs.
     */
    Collection<Bomb> getBombs();

    /**
     * Gets the door.
     * 
     * @return the door
     */
    Door getDoor();

    /**
     * Gets the key.
     * 
     * @return the key
     */
    Optional<PickableObject> getKey();

    /**
     * Gets the world's canvas.
     * 
     * @return the canvas of the world
     */
    WorldGraphicComponentImpl getGraphicComponent();

    /**
     * Gets the game context handler.
     * 
     * @return the game context handler.
     */
    GameContext getGameContext();

    /**
     * Gets the game timer.
     * 
     * @return world's timer
     */
    GameTimer getGameTimer();

    /**
     * Gets the player's score.
     * 
     * @return the score of the player
     */
    PlayerScore getPlayerScore();

    /**
     * Adds a bomb in the world.
     * 
     * @param bomb the bomb to add
     * @return if the bomb has been added or not
     */
    boolean addBomb(Bomb bomb);

    /**
     * Adds the flames in the world.
     * 
     * @param flames the set of flames to add
     */
    void addFlames(Collection<Flame> flames);

    /**
     * Removes a game object from the world.
     * 
     * @param object the object to remove
     * @param <X> object's type to remove
     */
    <X extends GameObject> void removeObject(X object);
}
