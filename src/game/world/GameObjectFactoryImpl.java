package game.world;

import game.object.Solidity;
import game.objects.blocks.Block;
import game.objects.blocks.Door;
import game.objects.blocks.PickableObject;
import game.objects.entities.BalloomEnemy;
import game.objects.entities.GhostEnemy;
import game.objects.entities.PlayerImpl;
import game.objects.entities.SmartEnemy;
import game.theme.Theme;
import game.utilities.Position;

/**
 * This class models {@link GameObjectFactory}.
 */
public class GameObjectFactoryImpl implements GameObjectFactory {

    private final Theme theme;

    /**
     * Creates a {@code GameObjectFactoryImpl}.
     * 
     * @param theme the theme of objects' sprite
     */
    public GameObjectFactoryImpl(final Theme theme) {
        this.theme = theme;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PlayerImpl createPlayer(final Position position, final World world) {
        return new PlayerImpl(new Position(position), Solidity.BREAKABLE, this.theme.getSprites().getPlayerSprite(), world);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BalloomEnemy createBalloomEnemy(final Position position, final World world) {
        return new BalloomEnemy(new Position(position), Solidity.BREAKABLE, world, this.theme.getSprites().getEnemySprite());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GhostEnemy createGhostEnemy(final Position position, final World world) {
        return new GhostEnemy(new Position(position), Solidity.BREAKABLE, world, this.theme.getSprites().getGhost());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SmartEnemy createSmartEnemy(final Position position, final World world) {
        return new SmartEnemy(new Position(position), Solidity.BREAKABLE, world, this.theme.getSprites().getSmartEnemy());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Block createUnbreakableBlock(final Position position) {
        return new Block(Solidity.UNBREAKABLE, new Position(position), this.theme.getSprites().getWallSprite());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Block createBreakableBlock(final Position position) {
        return new Block(Solidity.BREAKABLE, new Position(position), this.theme.getSprites().getBreakableWallSprite());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Door createDoor(final Position position) {
        return new Door(new Position(position), this.theme.getSprites().getDoorSprite(), this.theme.getSounds().getDoorSound());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PickableObject createKey(final Position position) {
        return new PickableObject(new Position(position), player -> player.takeKey(), this.theme.getSprites().getKeySprite());
    }

}
