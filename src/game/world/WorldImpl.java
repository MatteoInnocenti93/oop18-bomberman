package game.world;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import game.graphics.SpriteSheet;
import game.object.GameObject;
import game.object.Solidity;
import game.objects.blocks.Block;
import game.objects.blocks.Bomb;
import game.objects.blocks.Door;
import game.objects.blocks.Flame;
import game.objects.blocks.PickableObject;
import game.objects.entities.Enemy;
import game.objects.entities.PlayerImpl;
import game.state.GameContext;
import game.theme.Theme;
import game.utilities.FrameSizeUtils;
import game.utilities.GameTimer;
import game.utilities.PlayerScore;
import game.utilities.PlayerScoreImpl;
import game.utilities.Position;
import menu.frame.WorldGraphicComponentImpl;

/**
 * This class manages the whole game world.
 */
public class WorldImpl implements World {

    /**
     * The lower bound in the world.
     */
    public static final int LOWER_BOUND = 1;
    /**
     * The upper bound in the world.
     */
    public static final int UPPER_BOUND = FrameSizeUtils.NUM_TILES - 1;
    /**
     * The upper bound in pixel.
     */
    public static final int UPPER_BOUND_IN_PIXEL = FrameSizeUtils.getEdgeLength() - (2 * SpriteSheet.SPRITE_SIZE_IN_GAME);
    /**
     * The lower bound in pixel.
     */
    public static final int LOWER_BOUND_IN_PIXEL = 0 + SpriteSheet.SPRITE_SIZE_IN_GAME;
    private static final int N_BOMBS = 5;
    private final Theme theme;
    private final Set<Block> blocks;
    private final Set<Position> freeTiles;
    private final Set<Enemy> enemies;
    private final BlockingQueue<Bomb> bombs;
    private final BlockingQueue<Flame> flames;
    private Door door;
    private final Set<PickableObject> pickableObjects;
    private Optional<PickableObject> key;
    private PlayerImpl player;
    private final WorldGraphicComponentImpl graphicComponent;
    private final GameContext gameContext;
    private final GameTimer timer;
    private final PlayerScore score;

    /**
     * Creates a {@code WorldImpl}.
     *
     * @param theme the theme of the sprite
     * @param gameContext the game context handler.
     */
    public WorldImpl(final Theme theme, final GameContext gameContext) {
        this.theme = theme;
        this.gameContext = gameContext;
        this.freeTiles = new HashSet<>();
        this.blocks = new HashSet<>();
        this.enemies = new HashSet<>();
        this.bombs = new ArrayBlockingQueue<>(N_BOMBS);
        this.flames = new LinkedBlockingQueue<>();
        this.pickableObjects = new HashSet<>();
        this.createLevel();
        this.graphicComponent = new WorldGraphicComponentImpl(this);
        this.timer = new GameTimer();
        this.score = new PlayerScoreImpl();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Theme getTheme() {
        return this.theme;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Position> getFreePosition() {
        return Collections.unmodifiableSet(this.freeTiles);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<? extends GameObject> getAllGameObjects() {
        final List<GameObject> allGameObjects = new LinkedList<>();
        allGameObjects.add(this.door);
        allGameObjects.addAll(this.pickableObjects);
        allGameObjects.addAll(this.blocks);
        allGameObjects.addAll(this.bombs);
        allGameObjects.addAll(this.enemies);
        allGameObjects.add(this.player);
        allGameObjects.addAll(this.flames);
        return Collections.unmodifiableList(allGameObjects);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Block> getBlocks() {
        return Collections.unmodifiableSet(this.blocks);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PlayerImpl getPlayer() {
        return this.player;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Bomb> getBombs() {
        return Collections.unmodifiableCollection(this.bombs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Door getDoor() {
        return this.door;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Optional<PickableObject> getKey() {
        return this.key;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public WorldGraphicComponentImpl getGraphicComponent() {
        return this.graphicComponent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GameContext getGameContext() {
        return this.gameContext;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GameTimer getGameTimer() {
        return this.timer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PlayerScore getPlayerScore() {
        return this.score;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <X extends GameObject> void removeObject(final X object) {
        if (this.blocks.contains(object)) {
            this.freeTiles.add(new Position(object.getPosition()));
            this.blocks.remove(object);
        } else if (this.pickableObjects.contains(object)) {
            this.pickableObjects.remove(object);
            if (this.key.isPresent() && this.key.get().equals(object)) {
                this.key = Optional.empty();
            }
        } else if (this.enemies.contains(object)) {
            this.enemies.remove(object);
            this.score.addScore(((Enemy) object).getEnemyScore());
        } else if (this.bombs.contains(object)) {
            this.bombs.poll();
        } else if (this.flames.contains(object)) {
            this.flames.remove(object);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(final long elapsedTime) {
        if (!this.theme.getSounds().getMusicSound().isPlaying()) {
            this.theme.getSounds().getMusicSound().play();
        }
        this.enemies.forEach(e -> e.update(elapsedTime));
        this.player.update(elapsedTime);
        this.bombs.forEach(e -> e.update(elapsedTime));
        this.flames.forEach(e -> e.update(elapsedTime));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render() {
        this.graphicComponent.render();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean addBomb(final Bomb bomb) {
        return this.bombs.add(bomb);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addFlames(final Collection<Flame> flames) {
        this.flames.addAll(flames);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processInput() {
        this.player.processInput();
        this.enemies.forEach(e -> e.processInput());
    }

    private void createLevel() {
        final WorldInitializer initializer = new WorldInitializerImpl(this.theme);
        this.freeTiles.addAll(initializer.initializeFreeTiles());
        this.blocks.addAll(initializer.initializeUnbreakableBlocks());
        final Set<Block> breakableBlocks = initializer.initializeBreakableBlocks(this.freeTiles);
        this.freeTiles.removeAll(breakableBlocks.stream().map(b -> b.getPosition()).collect(Collectors.toSet()));
        this.blocks.addAll(breakableBlocks);
        this.key = Optional.ofNullable(initializer.initializeKey(this.blocks.stream()
                                                                       .filter(b -> b.isBreakable()
                                                                               == Solidity.BREAKABLE)
                                                                       .collect(Collectors.toSet())));
        this.pickableObjects.add(this.key.get());
        this.door = initializer.initializeDoor(this.blocks.stream()
                                                          .filter(b -> b.isBreakable()
                                                                  == Solidity.BREAKABLE)
                                                          .filter(b -> !this.pickableObjects.stream()
                                                                                  .filter(o ->
                                                                                  o.getPosition().equals(b.getPosition()))
                                                                                  .findAny()
                                                                                  .isPresent())
                                                          .collect(Collectors.toSet()));
        this.player = initializer.initializePlayer(this);
        this.enemies.addAll(initializer.initializeEnemies(this.freeTiles, this));
    }
}
