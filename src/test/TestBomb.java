package test;

import java.io.IOException;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.junit.Assert;
import org.junit.Test;

import game.graphics.SpriteSheet;
import game.object.GameObject;
import game.objects.blocks.Bomb;
import game.objects.blocks.Flame;
import game.theme.Theme;
import game.theme.ThemeImpl;
import game.utilities.Position;
import game.world.World;
import game.world.WorldImpl;

/**
 * Tests principal features of bomb and flame.
 */
public class TestBomb {

    private static final int GAME_TILE_TEST = SpriteSheet.SPRITE_SIZE_IN_GAME;
    // It must not be equal to or greater than half the size of the playing tile.
    private static final int NUMBER_TO_BE_ROUNDED = 2;
    private static final int X_POSITION_TO_BE_ROUND_DOWN = GAME_TILE_TEST + NUMBER_TO_BE_ROUNDED;
    private static final int Y_POSITION_TO_BE_ROUND_DOWN = GAME_TILE_TEST + NUMBER_TO_BE_ROUNDED;
    private static final int X_POSITION_TO_BE_ROUND_UP = GAME_TILE_TEST - NUMBER_TO_BE_ROUNDED;
    private static final int Y_POSITION_TO_BE_ROUND_UP = GAME_TILE_TEST - NUMBER_TO_BE_ROUNDED;
    private static final int X_POSITION_ROUNDED_DOWN = GAME_TILE_TEST;
    private static final int Y_POSITION_ROUNDED_DOWN = GAME_TILE_TEST;
    private static final int X_POSITION_ROUNDED_UP = GAME_TILE_TEST;
    private static final int Y_POSITION_ROUNDED_UP = GAME_TILE_TEST;
    private static final int BOMB_LIFETIME = 3000;
    private static final int BOMB_LIFETIME_UPPER_BOUND = 3100;
    private static final int BOMB_LIFETIME_LOWER_TIME = BOMB_LIFETIME - 1;
    private static final long MIN_GAMELOOP_ELAPSED_TIME = 10;
    private static final String GAME_THEME = "/ZeldaTheme";

    /**
     * Test to verify the in tile correct positioning of the bomb.
     */
    @Test
    public void testBombPosition() {
        // Bomb and Bomb Position initialization.
        Position playerPosition = new Position(X_POSITION_TO_BE_ROUND_DOWN, Y_POSITION_TO_BE_ROUND_DOWN);
        Position bombPosition = new Position(playerPosition.setInTile(playerPosition));
        // Rounded down position test.
        Assert.assertEquals(bombPosition, new Position(X_POSITION_ROUNDED_DOWN, Y_POSITION_ROUNDED_DOWN));
        playerPosition = new Position(X_POSITION_TO_BE_ROUND_UP, Y_POSITION_TO_BE_ROUND_UP);
        bombPosition = new Position(playerPosition.setInTile(playerPosition));
        // Rounded up position test.
        Assert.assertEquals(bombPosition, new Position(X_POSITION_ROUNDED_UP, Y_POSITION_ROUNDED_UP));
    }

    /**
     * Range time bomb explosion test.
     * 
     * @throws LineUnavailableException 
     * @throws IOException 
     * @throws UnsupportedAudioFileException 
     * 
     */
    @Test
    public void testBombExplosion() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        // World initialization.
        final Theme theme = new ThemeImpl(GAME_THEME);
        final World world = new WorldImpl(theme, null);
        final Random random = new Random();
        world.getGraphicComponent().dispose();
        world.addBomb(new Bomb(new Position(0, 0), world.getTheme().getSprites().getBomb(), world));
        int totalElapsedTime = 0;
        // GameLoop simulation.
        while (!firstEpxlosion(totalElapsedTime) && totalElapsedTime <= BOMB_LIFETIME_UPPER_BOUND) {
            final long elapsedTimeSimulate = random.nextInt(8) + MIN_GAMELOOP_ELAPSED_TIME; // 10 ÷ 17
            totalElapsedTime += elapsedTimeSimulate;
            world.update(elapsedTimeSimulate);
            if (totalElapsedTime <= BOMB_LIFETIME_LOWER_TIME) {
                // Bomb's life time is 3000 milliseconds, so it's still running and present in queue.
                Assert.assertFalse(world.getBombs().isEmpty());
            }
        }
        // Max Acceptable limit to extinguishes is 3100 milliseconds.
        Assert.assertTrue(totalElapsedTime <= BOMB_LIFETIME_UPPER_BOUND);
        // at about 3000 milliseconds more, bomb explodes and his relative queue is empty.
        Assert.assertTrue(world.getBombs().isEmpty());
        // Flames initialization test.
        final Set<GameObject> flameCheckSet = world.getAllGameObjects().stream().filter(f -> f instanceof Flame)
                .collect(Collectors.toSet());
        Assert.assertFalse(flameCheckSet.isEmpty());
    }

    /*
     * If the condition occurs, the bomb should be exploded. In this way I can
     * verify that at the first possibility that the bomb could explode, it really
     * explodes, making the test as faithful as possible.
     */
    private boolean firstEpxlosion(final int totalElapsedTime) {
        return totalElapsedTime >= BOMB_LIFETIME && totalElapsedTime <= BOMB_LIFETIME_UPPER_BOUND;
    }

}
