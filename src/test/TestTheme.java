package test;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.junit.Assert;
import org.junit.Test;

import game.theme.Theme;
import game.theme.ThemeImpl;
import game.world.World;
import game.world.WorldImpl;

/**
 * This class tests audio and theme loading.
 */
public class TestTheme {

    private static final long ELAPSED_TIME = 1500;
    private static final String THEME = "/KingdomHeartsTheme";
    private static final String WRONG_THEME = "/DragonBallTheme";

    /**
    * Test if music is played correctly.
    * 
    * @throws UnsupportedAudioFileException 
    * @throws IOException 
    * @throws LineUnavailableException 
    * @throws InterruptedException 
    */
   @Test
   public void testAudio() throws UnsupportedAudioFileException, IOException, LineUnavailableException, InterruptedException {
       final Theme theme = new ThemeImpl(THEME);
       theme.getSounds().getMusicSound().play();
       Thread.sleep(ELAPSED_TIME);
       Assert.assertTrue(theme.getSounds().getMusicSound().isPlaying());
       theme.getSounds().getMusicSound().stop();
   }

   /**
    * Tests a wrong string path where to load the theme.
    * 
    * @throws UnsupportedAudioFileException 
    * @throws LineUnavailableException 
    * @throws IOException 
    */
   @Test(expected = NullPointerException.class)
   public void testLoadingTheme() throws UnsupportedAudioFileException, LineUnavailableException, IOException {
       final Theme theme = new ThemeImpl(WRONG_THEME);
       final World world = new WorldImpl(theme, null);
       world.getGraphicComponent().dispose();
   }

}
