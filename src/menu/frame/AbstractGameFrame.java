/**
 * 
 */
package menu.frame;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import menu.frame.components.GameFont;

/**
 * This class provides a skeletal implementation of game frame and should be
 * implemented to standardize the game design. This class allows easy management
 * of the common basic settings of the game frame.
 */
public abstract class AbstractGameFrame extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final String TITLE = "Bomberman";
    private static final URL IMG_URL_ICON = ClassLoader.getSystemResource("MenuComponents/Images/Icon.png");
    private static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    private static final double SCALE = 2;

    /**
     * Creates {@code AbstractGameFrame} with default settings.
     */
    public AbstractGameFrame() {
        super();
        final int screenWidth;
        final int screenHeight;
        screenWidth = (int) (SCREEN_SIZE.getWidth() / SCALE);
        screenHeight = (int) (SCREEN_SIZE.getHeight() / SCALE);
        super.setTitle(TITLE);
        super.setIconImage(new ImageIcon(IMG_URL_ICON).getImage());
        super.setFont(new GameFont().getDefaultGameFont());
        super.setSize(screenWidth, screenHeight);
        super.setDefaultCloseOperation(EXIT_ON_CLOSE);
        super.setLocationByPlatform(true);
    }

    /**
     * Creates {@code AbstractGameFrame} with default settings and
     * {@link LayoutManager}.
     * 
     * @param manager the {@link LayoutManager}
     */
    public AbstractGameFrame(final LayoutManager manager) {
        this();
        super.setLayout(manager);
    }

    /**
     * Draws a {@link Component} in a {@link JPanel} with {@link GridBagLayout}
     * manager, specifying the distance that be implemented to setting same external
     * distance out of all {@link Component} borders. This method set the panel with
     * {@link GridBagLayout} manager.
     * 
     * @param panel the {@link JPanel} where to be insert the component
     * @param component the {@link Component} to be insert in the panel
     * @param distance the distance from external borders
     */
    protected void drawComponentInPanel(final JPanel panel, final Component component, final int distance) {
        final GridBagConstraints cnst = new GridBagConstraints();
        cnst.insets = new Insets(distance, distance, distance, distance);
        panel.setLayout(new GridBagLayout());
        panel.add(component, cnst);
    }

}
