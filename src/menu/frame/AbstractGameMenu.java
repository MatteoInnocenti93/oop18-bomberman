package menu.frame;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import game.state.GameContext;
import menu.frame.buttons.ExitButton;
import menu.frame.buttons.MainMenuButton;
import menu.frame.components.GamePanelImpl;

/**
 * This class provides a skeletal implementation of game menu where you can
 * choose two different choices: main menu or exits. This class is designed to
 * automatically scale down based of the main screen size. It implements
 * specific game components that make standard the overall design. For further
 * information see {@link AbstractGameFrame}, {@link GamePanelImpl},
 * {@link MainMenuButton} and {@link ExitButton} classes.
 */
public abstract class AbstractGameMenu extends AbstractGameFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final int DISTANCE_BUTTON = 20;

    /**
     * Creates {@code AbstractGameMenu} with default settings and specified
     * background image.
     * 
     * @param backgroundImage the background image to be load.
     * @param gameContext the {@link GameContext} handler.
     */
    public AbstractGameMenu(final GameContext gameContext, final ImageIcon backgroundImage) {
        super(new BorderLayout());
        final JPanel westInternalSouthPanel = new GamePanelImpl();
        final JPanel eastInternalSouthPanel = new GamePanelImpl();
        final JPanel southPanel = new GamePanelImpl(new BorderLayout());
        final JPanel northPanel = new GamePanelImpl(backgroundImage);
        drawComponentInPanel(westInternalSouthPanel, new MainMenuButton(gameContext), DISTANCE_BUTTON);
        drawComponentInPanel(eastInternalSouthPanel, new ExitButton(), DISTANCE_BUTTON);
        southPanel.add(westInternalSouthPanel, BorderLayout.WEST);
        southPanel.add(eastInternalSouthPanel, BorderLayout.EAST);
        this.getContentPane().add(northPanel, BorderLayout.NORTH);
        this.getContentPane().add(new GamePanelImpl(), BorderLayout.CENTER);
        this.getContentPane().add(southPanel, BorderLayout.SOUTH);
        super.pack();
        super.setResizable(false);
        super.setLocationRelativeTo(null);
    }

}
