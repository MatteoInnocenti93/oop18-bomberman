package menu.frame.components;

import java.awt.Color;

import javax.swing.JLabel;

/**
 * This models the label shown in the Game Pause menu.
 */
public class PauseMenuLabel extends AbstractGameLabel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String TITLE = "PAUSE";

    /**
     * Creates {@code PauseMenuLabel}.
     */
    public PauseMenuLabel() {
        super(TITLE);
        this.setForeground(Color.black);
        super.setHorizontalAlignment(JLabel.CENTER);
    }

}
