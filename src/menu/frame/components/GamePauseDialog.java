package menu.frame.components;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * This class models the dialog component of the Game Pause Menu.
 */
public class GamePauseDialog extends JDialog {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final float OPACITY = 0.7f;

    /**
     * Creates {@code GamePauseDialog}.
     *
     * @param frame that contains GamePauseDialog
     */
    public GamePauseDialog(final JFrame frame) {
        super(frame);
        this.setLayout(new BorderLayout());
        this.setUndecorated(true);
        this.setOpacity(OPACITY);
    }

}
