package menu.frame.components;

import javax.swing.JLabel;

/**
 * This class is the sign in info menu.
 */
public class InfoLabel extends AbstractGameLabel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String TITLE = "INFO";

    /**
     * Creates the {@code InfoLabel} for info window.
     */
    public InfoLabel() {
        super(TITLE);
        super.setHorizontalAlignment(JLabel.CENTER);
    }

}
