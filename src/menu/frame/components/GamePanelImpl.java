package menu.frame.components;

import java.awt.LayoutManager;

import javax.swing.Icon;

/**
 * This class extends {@link AbstractGamePanel}, and should be implemented to
 * standardize the game design. This class allows easy management of the common
 * basic settings of the game panel.
 */
public class GamePanelImpl extends AbstractGamePanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a {@code GamePanelImpl} with a double buffer, a flowLayout manager
     * and default settings.
     */
    public GamePanelImpl() {
        super();
    }

    /**
     * Creates a {@code GamePanelImpl} with the specified {@link LayoutManager}.
     * 
     * @param manager the {@link LayoutManager} to use
     */
    public GamePanelImpl(final LayoutManager manager) {
        super(manager);
    }

    /**
     * Creates a {@code GamePanelImpl} with the specified background {@link Icon}.
     * 
     * @param icon the {@link Icon} image to be load
     */
    public GamePanelImpl(final Icon icon) {
        super(icon);
    }

}
