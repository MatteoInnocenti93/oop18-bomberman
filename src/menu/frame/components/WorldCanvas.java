package menu.frame.components;


/**
 * This interface declares the methods of a world graphic component.
 */
public interface WorldCanvas {

    /**
     * Renders all objects that are in the world.
     */
    void render();
}
