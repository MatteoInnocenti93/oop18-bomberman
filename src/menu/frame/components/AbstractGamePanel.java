package menu.frame.components;

import java.awt.Color;
import java.awt.LayoutManager;

import javax.swing.Icon;
import javax.swing.JPanel;

/**
 * This class provides a skeletal implementation of game panel and should be
 * implemented to standardize the game design. This class allows easy management
 * of the common basic settings of the game panel.
 */
public abstract class AbstractGamePanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final Color BACKGROUND_COLOR = Color.BLACK;

    /**
     * Creates {@code AbstractGamePanel} with a double buffer, a flowLayout
     * manager and default settings.
     */
    public AbstractGamePanel() {
        super();
        super.setFont(new GameFont().getDefaultGameFont());
        super.setBackground(BACKGROUND_COLOR);
    }

    /**
     * Creates {@code AbstractGamePanel} with the specified
     * {@link LayoutManager}.
     * 
     * @param manager the {@link LayoutManager} to use
     */
    public AbstractGamePanel(final LayoutManager manager) {
        this();
        super.setLayout(manager);
    }

    /**
     * Creates {@code AbstractGamePanel} with the specified background
     * {@link Icon}.
     * 
     * @param icon the {@link Icon} image to be load
     */
    public AbstractGamePanel(final Icon icon) {
        this();
        super.add(new GameLabelImpl(icon));
    }

}
