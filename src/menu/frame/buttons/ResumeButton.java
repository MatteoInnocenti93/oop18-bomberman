package menu.frame.buttons;

import java.awt.Color;
import java.util.Optional;

import game.state.GameContext;

/**
 * This class models the Resume Button.
 */
public class ResumeButton extends AbstractGameButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String TEXT_BUTTON = "RESUME";

    /**
     * Creates Resume button.
     *
     * @param context game-state controller
     */
    public ResumeButton(final GameContext context) {
        super(TEXT_BUTTON);
        this.setForeground(Color.black);
        this.addActionListener((a) -> context.requestRunningState(Optional.empty()));
    }

}
