package menu.frame.buttons;

import java.awt.Color;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.KeyStroke;

import menu.frame.components.GameFont;
import menu.mouselisteners.MenuMouseListener;
import sounds.SoundImpl;

/**
 * This class provides a skeletal implementation of game button and should be
 * implemented to standardize the game design. This class allows easy management
 * of the common basic settings of the game button.
 */
public abstract class AbstractGameButton extends JButton {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final Color BACKGROUND_COLOR = Color.BLACK;
    private static final String SOUND_PATH = "/ChooseThemeMenu/select.wav";

    /**
     * The default menu button color.
     */
    public static final Color TEXT_BUTTON_COLOR = Color.YELLOW;

    /**
     * Creates {@code AbstractGameButton} with default setting.
     */
    public AbstractGameButton() {
        super();
        super.setFont(new GameFont().getDefaultGameFont());
        super.setForeground(TEXT_BUTTON_COLOR);
        super.setBackground(BACKGROUND_COLOR);
        super.setOpaque(true);
        super.setBorderPainted(false);
        super.setBorder(null);
        super.setContentAreaFilled(false);
        this.loadMouseListener();
        this.getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "none");
        this.getInputMap().put(KeyStroke.getKeyStroke("released SPACE"), "none");
    }

    /**
     * Creates {@code AbstractGameButton} with default setting and text.
     * 
     * @param textButton the button with text.
     */
    public AbstractGameButton(final String textButton) {
        this();
        super.setText(textButton);
    }

    /**
     * Creates {@code AbstractGameButton} with default setting and
     * {@link Icon}.
     * 
     * @param icon the {@link Icon} to display on the button.
     */
    public AbstractGameButton(final Icon icon) {
        this();
        super.setIcon(icon);
    }

    private void loadMouseListener() {
        try {
            this.addMouseListener(new MenuMouseListener(new SoundImpl(SOUND_PATH), this));
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

}
