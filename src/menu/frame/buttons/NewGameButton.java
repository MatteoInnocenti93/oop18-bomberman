package menu.frame.buttons;

import game.state.GameContext;

/**
 * This class extends {@link AbstractGameButton}. It allows easy management of
 * the commons behaviors and settings of the NewGame button in case of one or
 * multiple uses within its own program.
 */
public class NewGameButton extends AbstractGameButton {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String TEXT_BUTTON = "NEW GAME";

    /**
     * Creates a {@code NewGameButton}.
     * 
     * @param gameContext the {@link GameContext} handler
     */
    public NewGameButton(final GameContext gameContext) {
        super(TEXT_BUTTON);
        super.addActionListener(l -> gameContext.requestChooseThemeState());
    }

}
