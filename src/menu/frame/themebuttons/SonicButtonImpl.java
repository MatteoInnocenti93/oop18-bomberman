package menu.frame.themebuttons;

import java.awt.Image;
import java.util.Optional;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import game.state.GameContext;

/**
 * This class is the button to choose the Sonic theme in game and extends {@link AbstractThemeMenuButton}.
 */
public class SonicButtonImpl extends AbstractThemeMenuButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final int BUTTON_PROPORTION_SIZE = 3;
    private static final String FOLDER = "/ChooseThemeMenu";
    private static final String SONIC_FOLDER_THEME = "/SonicTheme";

    private final GameContext gameContext;

    /**
     * Creates {@code SonicButtonImpl}.
     *
     * @param frame to close when button is pressed
     * @param gameContext the game context handler.
     */
    public SonicButtonImpl(final JFrame frame, final GameContext gameContext) {
        super();
        this.gameContext = gameContext;
        final int buttonSize = frame.getWidth() / BUTTON_PROPORTION_SIZE;
        final ImageIcon sonic = new ImageIcon(new ImageIcon(
                                this.getClass().getResource(FOLDER + "/sonic.png"))
                                               .getImage().getScaledInstance(buttonSize,
                                                                             buttonSize,
                                                                             Image.SCALE_SMOOTH));
        final ImageIcon sonicLogo = new ImageIcon(new ImageIcon(
                                    this.getClass().getResource(FOLDER + "/sonicLogo.png"))
                                                   .getImage().getScaledInstance(buttonSize,
                                                                                 buttonSize,
                                                                                 Image.SCALE_SMOOTH));
        this.setIcon(sonic);
        this.loadMouseListener(sonicLogo);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void chooseTheme() {
        this.gameContext.requestRunningState(Optional.of(SONIC_FOLDER_THEME));
    }

}
