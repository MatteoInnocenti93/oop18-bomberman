package menu.frame.themebuttons;

import java.awt.Image;
import java.util.Optional;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import game.state.GameContext;

/**
 * This class is the button to choose the Saint Seiya theme in game and extends {@link AbstractThemeMenuButton}.
 */
public class SaintSeiyaButtonImpl extends AbstractThemeMenuButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final int BUTTON_PROPORTION_SIZE = 3;
    private static final String FOLDER = "/ChooseThemeMenu";
    private static final String SEIYA_FOLDER_THEME = "/SaintSeiyaTheme";

    private final GameContext gameContext;

    /**
     * Creates {@code SaintSeiyaButtonImpl}.
     *
     * @param frame to close when button is pressed
     * @param gameContext handler.
     */
    public SaintSeiyaButtonImpl(final JFrame frame, final GameContext gameContext) {
        super();
        this.gameContext = gameContext;
        final int buttonSize = frame.getWidth() / BUTTON_PROPORTION_SIZE;
        final ImageIcon saintSeiya = new ImageIcon(new ImageIcon(
                                 this.getClass().getResource(FOLDER + "/saintSeiya.png"))
                                                .getImage().getScaledInstance(buttonSize,
                                                                              buttonSize,
                                                                              Image.SCALE_SMOOTH));
        final ImageIcon saintSeiyaLogo = new ImageIcon(new ImageIcon(
                             this.getClass().getResource(FOLDER + "/saintSeiyaLogo.png"))
                                            .getImage().getScaledInstance(buttonSize,
                                                                          buttonSize,
                                                                          Image.SCALE_SMOOTH));
        this.setIcon(saintSeiya);
        this.loadMouseListener(saintSeiyaLogo);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void chooseTheme() {
        this.gameContext.requestRunningState(Optional.of(SEIYA_FOLDER_THEME));
    }

}
