package menu.frame;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

import game.state.GameContext;
import menu.frame.buttons.ExitButton;
import menu.frame.components.ChooseThemeLabelImpl;
import menu.frame.components.GamePanelImpl;
import menu.frame.themebuttons.BackButtonImpl;
import menu.frame.themebuttons.SaintSeiyaButtonImpl;
import menu.frame.themebuttons.SonicButtonImpl;
import menu.frame.themebuttons.SoraButtonImpl;
import menu.frame.themebuttons.ZeldaButtonImpl;

/**
 * This class is the choosing theme menu window, that shows a group of buttons relative to the game themes
 * you want to choose.
 */
public class ThemeMenu extends AbstractGameFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final int DISTANCE_LABEL = 30;
    private static final int DISTANCE_BUTTON = 20;

    private JPanel northPanel = new GamePanelImpl(new GridBagLayout());
    private JPanel centerPanel = new GamePanelImpl(new BorderLayout());
    private final GameContext gameContext;

    /**
     * Creates {@code ThemeMenu}.
     *
     * @param gameContext the {@link GameContext} handler
     */
    public ThemeMenu(final GameContext gameContext) {
        super(new BorderLayout());
        this.gameContext = gameContext;
        this.drawFrame();
        super.getContentPane().add(northPanel, BorderLayout.NORTH);
        super.getContentPane().add(centerPanel, BorderLayout.CENTER);
        super.setSize(this.getWidth(), this.getWidth());
        super.pack();
        super.setResizable(false);
        super.setLocationRelativeTo(null);
    }

    private void drawFrame() {
        final JPanel westCenterPanel = new GamePanelImpl(new BorderLayout());
        final JPanel eastCenterPanel = new GamePanelImpl(new BorderLayout());
        final JPanel westInternalSouthPanel = new GamePanelImpl();
        final JPanel eastInternalSouthPanel = new GamePanelImpl();
        this.northPanel = new GamePanelImpl();
        this.centerPanel = new GamePanelImpl(new BorderLayout());
        drawComponentInPanel(northPanel, new ChooseThemeLabelImpl(), DISTANCE_LABEL);
        drawComponentInPanel(westInternalSouthPanel, new BackButtonImpl(this.gameContext), DISTANCE_BUTTON);
        drawComponentInPanel(eastInternalSouthPanel, new ExitButton(), DISTANCE_BUTTON);
        westCenterPanel.add(new SoraButtonImpl(this, this.gameContext), BorderLayout.NORTH);
        westCenterPanel.add(new ZeldaButtonImpl(this, this.gameContext), BorderLayout.CENTER);
        westCenterPanel.add(westInternalSouthPanel, BorderLayout.SOUTH);
        eastCenterPanel.add(new SaintSeiyaButtonImpl(this, this.gameContext), BorderLayout.NORTH);
        eastCenterPanel.add(new SonicButtonImpl(this, this.gameContext), BorderLayout.CENTER);
        eastCenterPanel.add(eastInternalSouthPanel, BorderLayout.SOUTH);
        centerPanel.add(westCenterPanel, BorderLayout.WEST);
        centerPanel.add(eastCenterPanel, BorderLayout.EAST);
    }

}
