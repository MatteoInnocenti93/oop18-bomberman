package menu.mouselisteners;

import java.awt.Color;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

import sounds.Sound;

/**
 * Mouse listener to use in game menu.
 */
public class MenuMouseListener extends AbstractMouseListener {

    private static final Color TEXT_BUTTON_COLOR = Color.RED;

    private final JButton button;
    private Color buttonColor;

    /**
     * Creates a {@code MenuMouseListener}.
     *
     * @param sound the {@link Sound} to play when mouse is on button
     * @param button the button reference
     */
    public MenuMouseListener(final Sound sound, final JButton button) {
        super(sound);
        this.button = button;
    }

    /**
     * Plays the sound and changes the button text color.
     */
    @Override
    public void mouseEntered(final MouseEvent e) {
        super.playSound();
        this.buttonColor = this.button.getForeground();
        this.button.setForeground(TEXT_BUTTON_COLOR);
    }

    /**
     * Restores the button text default color.
     */
    @Override
    public void mouseExited(final MouseEvent e) {
        this.button.setForeground(this.buttonColor);
    }

    /**
     * Resets the initial text color when mouse click is released.
     */
    @Override
    public void mouseReleased(final MouseEvent e) {
        this.button.setForeground(this.buttonColor);
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
    }

    @Override
    public void mousePressed(final MouseEvent e) {
    }

}
